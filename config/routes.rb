Rails.application.routes.draw do
  devise_for :users, skip: :sessions
  namespace :api do
    namespace :v1 do
      post '/sessions/sign_in', to: 'sessions#sign_in'
      post '/sessions/sign_up', to: 'sessions#sign_up'
      delete '/sessions/sign_out', to: 'sessions#sign_out'
      post '/user_groups/create', to: 'user_groups#create'
      get '/user_groups', to: 'user_groups#index'
      get '/user_groups/:id/activate', to: 'user_groups#activate'
      get '/user_groups/:id', to: 'user_groups#show'
      post '/user_groups/:id/add_user', to: 'user_groups#add_user'
      post '/user_groups/:user_group_id/notes', to: 'notes#create'
      post '/users', to: 'user_groups#users'
    end
  end
end
