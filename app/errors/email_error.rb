# frozen_string_literal: true

class EmailError < ApplicationError
end
