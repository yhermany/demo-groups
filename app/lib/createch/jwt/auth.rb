# frozen_string_literal: true

require 'active_support/concern'

module Createch
  module Jwt
    # This must be included in the API ApplicationController.
    module Auth
      extend ActiveSupport::Concern
      included do
        attr_reader :current_user
        attr_reader :payload_extras

        protected

        def authenticate_request!
          payload = payload!
          data = payload['data']
          raise JwtError, 'No autenticado' unless data['user_id'].to_i
          raise JwtError, 'Su acceso ha sido revocado.' if JwtBlacklist.jwt_revoked?(payload)

          @current_user = User.find(data['user_id'])
          @payload_extras = data['extras']
        end

        def create_token(user_id, extras = {})
          token = JsonWebToken.encode(user_id: user_id, extras: extras)
          "Bearer #{token}"
        end

        def revoke_token
          JwtBlacklist.revoke_jwt(payload!)
        end

        def invalid_login_attempt(error)
          raise JwtError, error
        end

        def payload!
          Createch::Jwt::JsonWebToken.decode(http_token!)
        end

        private

        def http_token!
          if request.headers['Authorization'].present?
            request.headers['Authorization'].split(' ').last
          else
            raise JwtError, 'No autenticado'
          end
        end
      end
    end
  end
end
