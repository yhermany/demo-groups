# frozen_string_literal: true

require 'active_support/concern'

module Createch
  module Jwt
    # This strategy must be included in an ActiveRecord model, and requires
    # that it has a `jti` column.
    #
    # In order to tell whether a token is revoked, it just checks whether
    # `jti` is in the table. On revocation, creates a new record with it.
    module Blacklist
      extend ActiveSupport::Concern

      included do
        def self.jwt_revoked?(payload)
          exists?(jti: payload['jti'])
        end

        def self.revoke_jwt(payload)
          find_or_create_by!(jti: payload['jti'],
                             exp: Time.zone.at(payload['exp'].to_i))
        end
      end
    end
  end
end
