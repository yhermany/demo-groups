# frozen_string_literal: true

module Createch
  module Jwt
    class JsonWebToken
      ALG = 'HS256'
      PAYLOAD_POSITION = 0
      AUD_HEADER = 'JWT_AUD'

      def self.encode(data)
        exp = DateTime.now.to_i + ENV['JWT_EXP_SECONDS'].to_i
        iat = DateTime.now.to_i
        jti_raw = [ENV['JWT_SECRET_KEY'], iat].join(':').to_s
        jti = Digest::MD5.hexdigest(jti_raw)
        payload = { data: data, exp: exp, aud: AUD_HEADER, iat: iat, jti: jti }
        JWT.encode(payload, ENV['JWT_SECRET_KEY'], ALG)
      end

      def self.decode(token)
        decoded_token = JWT.decode(
          token, ENV['JWT_SECRET_KEY'], true,
          aud: AUD_HEADER, verify_aud: true, verify_iat: true,
          verify_jti: proc { |jti, payload| validate_jti(jti, payload) },
          algorithm: ALG
        )
        HashWithIndifferentAccess.new(decoded_token[PAYLOAD_POSITION])
      rescue StandardError
        raise JwtError, 'El token es inválido.'
      end

      def self.validate_jti(jti, payload)
        jti_raw = [ENV['JWT_SECRET_KEY'], payload['iat']].join(':').to_s
        original_jti = Digest::MD5.hexdigest(jti_raw)
        jti == original_jti
      end
    end
  end
end
