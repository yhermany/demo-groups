# frozen_string_literal: true

module Createch
  module Jwt
    class JwtError < StandardError
    end
  end
end
