# frozen_string_literal: true

module Api
  module V1
    class SessionsController < ApplicationController
      skip_before_action :authenticate_request!, except: [:sign_out]

      def sign_up
        user = User.find_for_database_authentication(email: params[:email])
        raise ApplicationError, 'El correo ingresado ya existe.' if user

        user = User.create!(email: params[:email], password: params[:password])
        json_response(
          token: create_token(user.id),
          user: user
        )
      end

      def sign_in
        user = User.find_for_database_authentication(email: params[:email])
        return invalid_login_attempt 'No existe el usuario.' unless user

        if user.valid_password?(params[:password])
          json_response(
            token: create_token(user.id),
            user: user
          )
        else
          invalid_login_attempt 'El correo/password es invalido.'
        end
      end

      def sign_out
        revoke_token
        json_response(message: 'Session eliminada')
      end
    end
  end
end
