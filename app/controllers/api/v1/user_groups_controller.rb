# frozen_string_literal: true

module Api
  module V1
    class UserGroupsController < ApplicationController
      before_action :set_user_group, only: [:activate, :add_user, :show]

      def users
        json_response(User.where.not(id: current_user.id))
      end

      def index
        json_response(current_user.user_groups)
      end

      def show
        json_response(
          { user_group: @user_group, notes: @user_group.group.notes }
        )
      end

      def edit; end

      def create
        ActiveRecord::Base.transaction do
          group = Group.create!(group_params.merge(user: current_user))
          user_group = current_user.user_groups.create!(group: group, is_active: false)
          json_response(user_group)
        end
      end

      def activate
        ActiveRecord::Base.transaction do
          if current_user.group_active? && !@user_group.is_active
            raise ApplicationError, 'Ya tiene un grupo activo.'
          end

          @user_group.update!(is_active: !@user_group.is_active)
          json_response(@user_group)
        end
      end

      def add_user
        ActiveRecord::Base.transaction do
          unless @user_group.group.user.id == current_user.id
            raise ApplicationError, 'No eres el creador del grupo no tienes permitido esta accion.'
          end

          user = User.find(params[:user_id])
          user.user_groups.create!(group: @user_group.group)
          json_response({ message: 'Agregado exitosamente.' })
        end
      end

      private

      def group_params
        params.require(:group).permit(:name)
      end

      def set_user_group
        @user_group = current_user.user_groups.find(params[:id])
      end
    end
  end
end
