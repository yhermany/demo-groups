# frozen_string_literal: true

module Api
  module V1
    class NotesController < ApplicationController
      before_action :set_user_group, only: [:create]

      def create
        ActiveRecord::Base.transaction do
          note = @user_group.group.notes.create!(note_params.merge(user: current_user))
          json_response(note)
        end
      end

      private

      def note_params
        params.require(:note).permit(:title, :detail)
      end

      def set_user_group
        @user_group = current_user.user_groups.find(params[:user_group_id])
      end
    end
  end
end
