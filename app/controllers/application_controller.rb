# frozen_string_literal: true

class ApplicationController < ActionController::API
  include ExceptionHandler
  include Response
  include Createch::Jwt::Auth
  before_action :authenticate_request!
end
