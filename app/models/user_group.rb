# frozen_string_literal: true

class UserGroup < ApplicationRecord
  belongs_to :user
  belongs_to :group

  validates :user_id, uniqueness: { scope: :group_id, message: 'El usuario ya esta es miembro del grupo.' }

  def as_json(*)
    super.except('group_id').tap do |hash|
      hash[:group] = {
        id: group.id,
        name: group.name,
        creator: {
          id: group.user.id,
          email: group.user.email
        }
      }
    end
  end
end
