# frozen_string_literal: true

class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable

  has_many :user_groups, dependent: :destroy
  has_many :groups, through: :user_groups

  def self.find_by_email(email)
    User.where('lower(email) = ?', email.downcase).first
  end

  def self.find_by_email!(email)
    User.where('lower(email) = ?', email.downcase).first!
  end

  def group_active?
    user_groups.each do |user_group|
      return true if user_group.is_active
    end
    false
  end
end
