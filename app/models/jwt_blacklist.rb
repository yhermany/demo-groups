# frozen_string_literal: true

class JwtBlacklist < ApplicationRecord
  include Createch::Jwt::Blacklist
  self.table_name = 'jwt_blacklist'
end
