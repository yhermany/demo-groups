# frozen_string_literal: true

class Note < ApplicationRecord
  belongs_to :user
  belongs_to :group
  validates :title, presence: true

  def as_json(*)
    super.except('user_id').tap do |hash|
      hash[:user] = {
        id: user.id,
        email: user.email
      }
    end
  end
end
